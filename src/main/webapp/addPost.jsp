<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="flex.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <%@include file="side_menu.jsp" %>
        <div id="form">
            <form name="addPost" action="addPost.do" method="POST">
                <div class="row"><input type="text" name="blogPostName" value="" placeholder="Tytul"/></div>
                <div class="row"><textarea name="blogPostText" rows="20" cols="50"></textarea></div>
                <div class="row"><input type="text" name="authorName" value="" placeholder="Imie autora"/></div>
                <div class="row"><input type="submit" value="Dodaj" name="dodajButton" /></div>
            </form>
        </div>
        <%@include file="footer.jsp" %>

                    