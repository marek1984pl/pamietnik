<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<!DOCTYPE html>
<html>
    <head>
        <link href="flex.css" rel="stylesheet">
        <meta http-equiv="Content-Type" content="text/html; charset=UTF-8">
        <title>JSP Page</title>
    </head>
    <body>
        <%@include file="header.jsp" %>
        <%@include file="side_menu.jsp" %>
        <main>
            <h1>About me</h1>
            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur nec felis orci. Cras eget enim at lorem semper imperdiet. Nulla interdum, turpis sed ullamcorper rhoncus, lectus ligula auctor ante, nec commodo risus diam at odio. Vestibulum molestie lacinia mi. Etiam sed dignissim purus. Vivamus ex orci, eleifend ac mollis sed, elementum at metus. Sed id urna risus. Ut vulputate arcu erat, eget porttitor nibh pellentesque eu. Fusce tincidunt magna eu elit tristique, et ornare ante hendrerit. Cras vitae dolor metus. Vestibulum aliquet, ante vitae tincidunt tempor, velit leo semper elit, sit amet consequat ipsum sem blandit nulla. Nullam vehicula purus ut aliquam maximus.</p>
        </main>
        <%@include file="footer.jsp" %>
