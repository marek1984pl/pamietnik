<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<%@ taglib prefix="fmt" uri="http://java.sun.com/jsp/jstl/fmt" %>

<main>
    <c:forEach items="${posts}" var="post">
        <div class="post">
            <h1 class="post"><c:out value="${post.title}" /></h1>
            <h2 class="post">Autor: <c:out value="${post.user.username}" /></h2>
            <h2 class="post"><fmt:formatDate type="date" value="${post.createDate}" pattern="HH:mm | dd MMMM yyyy" /></h2>
            <p class="post"><c:out value="${post.text}" /></p>
        </div>
    </c:forEach>
</main>