<%@page contentType="text/html" pageEncoding="UTF-8"%>
<%@taglib prefix="c" uri="http://java.sun.com/jsp/jstl/core" %>
<div id = "container">
    <header>
        <h1>Mój blog o programowaniu</h1>
        <div id = "motto">Blog kurs sapiens</div>
    </header>
    <div id = "content">
        <div id= "menu">
            <ul>
                <a href="/pamietnik"><li>Home</li></a>
                <a href="guestBook.jsp"><li>Ksiega gosci</li></a>
                <a href="aboutMe.jsp"><li>O mnie</li></a>
                <a href="addPost.jsp"><li>Dodaj post</li></a>
            </ul>
        </div>

