/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package pl.sda.standalone.pamietnik;

import java.io.IOException;
import java.io.PrintWriter;
import java.util.Date;
import java.util.List;
import javax.servlet.ServletException;
import javax.servlet.annotation.WebServlet;
import javax.servlet.http.HttpServlet;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import org.hibernate.Query;
import org.hibernate.Session;
import org.hibernate.SessionFactory;
import org.hibernate.Transaction;
import pl.sda.standalone.pamietnik.model.Post;
import pl.sda.standalone.pamietnik.model.User;

/**
 *
 * @author RENT
 */
@WebServlet(name = "AddPostServlet", urlPatterns = {"/addPost.do"})
public class AddPostServlet extends HttpServlet {

    /**
     * Processes requests for both HTTP <code>GET</code> and <code>POST</code>
     * methods.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    protected void processRequest(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        response.setContentType("text/html;charset=UTF-8");
           

    }

    // <editor-fold defaultstate="collapsed" desc="HttpServlet methods. Click on the + sign on the left to edit the code.">
    /**
     * Handles the HTTP <code>GET</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        request.getRequestDispatcher("/addPost.jsp").forward(request, response);
    }

    /**
     * Handles the HTTP <code>POST</code> method.
     *
     * @param request servlet request
     * @param response servlet response
     * @throws ServletException if a servlet-specific error occurs
     * @throws IOException if an I/O error occurs
     */
    @Override
    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        
        response.setContentType("text/html;charset=UTF-8");
        
        SessionFactory instance = ConfigHibernate.getInstance();
        Session session = instance.openSession();
        Transaction transaction = session.beginTransaction();
        
        //todo poprawic dodawanie userow
        Post post = new Post();
        User user = new User();
        
        try {
            user.setFirstName("xxx");
            user.setLastName("yyy");
            user.setUsername(request.getParameter("authorName"));
            
            session.save(user);
            if (!transaction.wasCommitted())
                    transaction.commit();
        }
        catch (Exception ex) {
            transaction.rollback();
        }
        finally {
             session.close();
        }
                
        try {
            //Query query = session.createQuery("from User where username " + request.getParameter("authorName"));
            //List<User> listOfUsers = query.list();

            //if (!listOfUsers.isEmpty()) {
            //    user = listOfUsers.get(0);
            //} else {
            //    user.setUsername(request.getParameter("authorName"));
            //}           
            
            session = instance.openSession();
            transaction = session.beginTransaction();
            
            post.setTitle(request.getParameter("blogPostName"));
            post.setText(request.getParameter("blogPostText"));
            post.setCreateDate(new Date());
            post.setEditDate(new Date());
            post.setUser(user);

            session.save(post);
            if (!transaction.wasCommitted()) {
                transaction.commit();
            }
        }
        catch (Exception ex) {
            transaction.rollback();
        }
        finally {
            session.close();
        }

        request.getRequestDispatcher("/index.do").forward(request, response);
    }

    /**
     * Returns a short description of the servlet.
     *
     * @return a String containing servlet description
     */
    @Override
    public String getServletInfo() {
        return "Short description";
    }// </editor-fold>

}
